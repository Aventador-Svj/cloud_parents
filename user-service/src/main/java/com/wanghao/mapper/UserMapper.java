package com.wanghao.mapper;

import com.wanghao.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 97898
* @description 针对表【tb_user】的数据库操作Mapper
* @createDate 2023-12-08 09:01:21
* @Entity com.wanghao.pojo.User
*/
public interface UserMapper extends BaseMapper<User> {

}




