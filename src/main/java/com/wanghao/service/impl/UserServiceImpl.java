package com.wanghao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanghao.pojo.User;
import com.wanghao.service.UserService;
import com.wanghao.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
* @author 97898
* @description 针对表【tb_user】的数据库操作Service实现
* @createDate 2023-12-08 09:01:00
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

}




