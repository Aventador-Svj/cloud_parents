package com.wanghao.service;

import com.wanghao.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 97898
* @description 针对表【tb_user】的数据库操作Service
* @createDate 2023-12-08 09:01:00
*/
public interface UserService extends IService<User> {

}
